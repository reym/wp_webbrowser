import unittest
from init import request

class TestBrowser(unittest.TestCase):

    def test_request_file(self):
        file = open("test/index.html", "r").read()
        _, body = request(
            "file:///home/antoni/Projects/webbrowser/wp_webbrowser/test/index.html"
        )
        self.assertEqual(body, file)
