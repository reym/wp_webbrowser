import os
import socket
import ssl
import tkinter
import tkinter.font
from types import NoneType
from typing import Any, Optional
from parser.html_parser import Element, HTMLParser, Text, print_tree
from parser.css_parser import CSSParser, cascade_priority
from layout.layout import WIDTH, HEIGHT, get_font, DocumentLayout

SCROLL_STEP = 100
SUPPORTED_SCHEMA = ["http", "https", "file"]
INHERITED_PROPERTIES = {
    "font-size": "16px",
    "font-style": "normal",
    "font-weight": "normal",
    "color": "black",
}
# space where browser chrome is to avoid drawing on top of it
CHROME_PX = 100


def request(url: str):
    """makes a http GET request and returns the recieved headers and body."""
    scheme, url = url.split("://", 1)
    assert scheme in SUPPORTED_SCHEMA, "Unknown scheme {}".format(scheme)

    if scheme == "file":
        file = open(url, "r")
        body = file.read()
        return {}, body

    if "/" in url:
        host, path = url.split("/", 1)
        path = "/" + path
    else:
        host = url
        path = "/"

    port = 80 if scheme == "http" else 443

    if ":" in host:
        host, port = host.split(":", 1)
        port = int(port)

    s = socket.socket(
        family=socket.AF_INET,
        type=socket.SOCK_STREAM,
        proto=socket.IPPROTO_TCP,
    )
    s.connect((host, port))

    if scheme == "https":
        ctx = ssl.create_default_context()
        s = ctx.wrap_socket(s, server_hostname=host)

    s.send(
        "GET {} HTTP/1.0\r\n".format(path).encode("utf8")
        + "Host: {}\r\n\r\n".format(host).encode("utf8")
    )

    response = s.makefile("r", encoding="utf8", newline="\r\n")

    statusline = response.readline()
    version, status, explanation = statusline.split(" ", 2)
    assert status == "200", "{}: {}".format(status, explanation)

    headers = {}
    while True:
        line = response.readline()
        if line == "\r\n":
            break
        header, value = line.split(":", 1)
        headers[header.lower()] = value.strip()

    assert "transfer-encoding" not in headers
    assert "content-encoding" not in headers

    body = response.read()
    s.close()

    return headers, body


def resolve_url(url, current):
    if "://" in url:
        return url
    elif url.startswith("/"):
        scheme, hostpath = current.split("://", 1)
        host, oldpath = hostpath.split("/", 1)
        return scheme + "://" + host + url
    else:
        dir, _ = current.rsplit("/", 1)
        while url.startswith("../"):
            url = url[3:]
            if dir.count("/") == 2:
                continue
            dir, _ = dir.rsplit("/", 1)
        return dir + "/" + url


def tree_to_list(tree, list: list[Any]) -> list[Any]:
    list.append(tree)
    for child in tree.children:
        tree_to_list(child, list)
    return list


def compute_style(node, property: str, value) -> Optional[str]:
    if property == "font-size":
        if value.endswith("px"):
            return value
        elif value.endswith("%"):
            if node.parent:
                parent_font_size = node.parent.style["font-size"]
            else:
                parent_font_size = INHERITED_PROPERTIES["font-size"]
            node_pct = float(value[:-1]) / 100
            parent_px = float(parent_font_size[:-2])
            return str(node_pct * parent_px) + "px"
        else:
            return None
    else:
        return value


def style(node, rules):
    node.style = {}
    for property, default_value in INHERITED_PROPERTIES.items():
        if node.parent:
            node.style[property] = node.parent.style[property]
        else:
            node.style[property] = default_value
    for selector, body in rules:
        if not selector.matches(node):
            continue
        for property, value in body.items():
            computed_value = compute_style(node, property, value)
            if not computed_value:
                continue
            node.style[property] = computed_value
    if isinstance(node, Element) and "style" in node.attributes:
        pairs = CSSParser(node.attributes["style"]).body()
        for property, value in pairs.items():
            computed_value = compute_style(node, property, value)
            node.style[property] = computed_value
    for child in node.children:
        style(child, rules)


class Tab:
    def __init__(self):
        self.display_list = []
        self.history = []

        with open("resources/browser.css") as f:
            self.default_style_sheet = CSSParser(f.read()).parse()

    def load(self, url: str):
        """Function to load a tab."""
        self.history.append(url)
        self.url = url
        self.scroll = 0

        headers, body = request(url)
        self.nodes = HTMLParser(body).parse()  # is of type Element or Text
        print_tree(self.nodes)

        rules = self.default_style_sheet.copy()
        links = [
            node.attributes["href"]
            for node in tree_to_list(self.nodes, [])
            if isinstance(node, Element)
            and node.tag == "link"
            and "href" in node.attributes
            and node.attributes.get("rel") == "stylesheet"
        ]
        for link in links:
            try:
                header, body = request(resolve_url(link, url))
            except:
                continue
            rules.extend(CSSParser(body).parse())
        style(self.nodes, sorted(rules, key=cascade_priority))

        self.document = DocumentLayout(self.nodes)
        self.document.layout()
        self.display_list = []
        self.document.paint(self.display_list)

    def draw(self, canvas):
        for cmd in self.display_list:
            # this avoids drawing the browser chrome to be drawn above the page
            if cmd.top > self.scroll + HEIGHT - CHROME_PX:
                continue
            if cmd.bottom < self.scroll:
                continue
            cmd.execute(self.scroll - CHROME_PX, canvas)

    def go_back(self):
        if len(self.history) > 1:
            self.history.pop()
            back = self.history.pop()
            self.load(back)

    def scrolldown(self):
        max_y = self.document.height - (HEIGHT - CHROME_PX)
        self.scroll = min(self.scroll + SCROLL_STEP, max_y)

    def scrollup(self):
        min_y = CHROME_PX
        self.scroll = max(self.scroll - SCROLL_STEP, min_y)

    def scroll_home(self, e):
        self.scroll = 0

    def click(self, x, y):
        y += self.scroll
        objs = [
            obj
            for obj in tree_to_list(self.document, [])
            if obj.x <= x < obj.x + obj.width and obj.y <= y < obj.y + obj.height
        ]
        if not objs:
            return
        elt = objs[-1].node
        while elt:
            if isinstance(elt, Text):
                pass
            elif elt.tag == "a" and "href" in elt.attributes:
                url = resolve_url(elt.attributes["href"], self.url)
                return self.load(url)
            elt = elt.parent


class Browser:
    def __init__(self):
        self.window = tkinter.Tk()
        self.canvas = tkinter.Canvas(
            self.window, width=WIDTH, height=HEIGHT, bg="white"
        )
        self.canvas.pack()
        self.url = None
        self.window.bind("<Down>", self.handle_down)
        self.window.bind("<Up>", self.handle_up)
        self.window.bind("j", self.handle_down)
        self.window.bind("k", self.handle_up)
        self.window.bind("<Button-1>", self.handle_click)

        with open("resources/browser.css", "r") as f:
            self.default_style_sheet = CSSParser(f.read()).parse()
        self.tabs: list[Tab] = []
        self.active_tab = 0
        self.window.bind("<Down>", self.handle_down)
        self.window.bind("<Button-1>", self.handle_click)

        # wether the address_bar is focues
        self.focus = None
        # contents of the address_bar
        self.address_bar = ""

        # key to capture all input
        self.window.bind("<Key>", self.handle_key)

        self.window.bind("<Return>", self.handle_enter)

    def load(self, url: str):
        """Initial function to _load_ a webpage."""
        new_tab = Tab()
        new_tab.load(url)
        self.active_tab = len(self.tabs)
        self.tabs.append(new_tab)
        self.draw()

    def draw(self):
        """actually render to the screen"""
        self.canvas.delete("all")
        self.tabs[self.active_tab].draw(self.canvas)
        # draw over letters that stick out from page
        self.canvas.create_rectangle(
            0, 0, WIDTH, CHROME_PX, fill="white", outline="black"
        )

        tabfont = get_font(20, "normal", "roman")
        for i, tab in enumerate(self.tabs):
            name = "Tab {}".format(i)
            x1, x2 = 40 + 80 * i, 120 + 80 * i
            # create broders around "Tab" (title)
            self.canvas.create_line(x1, 0, x1, 40, fill="black")
            self.canvas.create_line(x2, 0, x2, 40, fill="black")
            self.canvas.create_text(
                x1 + 10, 10, anchor="nw", text=name, font=tabfont, fill="black"
            )
            if i == self.active_tab:
                self.canvas.create_line(0, 40, x1, 40, fill="black")
                self.canvas.create_line(x2, 40, WIDTH, 40, fill="black")

        # Button to create a new tab
        buttonfont = get_font(20, "normal", "roman")
        self.canvas.create_rectangle(10, 10, 30, 30, outline="black", width=1)
        self.canvas.create_text(
            11, 0, anchor="nw", text="+", font=buttonfont, fill="black"
        )
        # address bar with current url
        self.canvas.create_rectangle(40, 50, WIDTH - 10, 90, outline="black", width=1)
        url = self.tabs[self.active_tab].url
        self.canvas.create_text(
            55, 55, anchor="nw", text=url, font=buttonfont, fill="black"
        )
        # back button
        self.canvas.create_rectangle(10, 50, 35, 90, outline="black", width=1)
        self.canvas.create_polygon(15, 70, 30, 55, 30, 85, fill="black")

        # check if current url or address bar focus should be rendered
        if self.focus == "address bar":
            self.canvas.create_text(
                55,
                55,
                anchor="nw",
                text=self.address_bar,
                font=buttonfont,
                fill="black",
            )
            w = buttonfont.measure(self.address_bar)
            self.canvas.create_line(55 + w, 55, 55 + w, 85, fill="black")
        else:
            url = self.tabs[self.active_tab].url
            self.canvas.create_text(
                55, 55, anchor="nw", text=url, font=buttonfont, fill="black"
            )

    # # Input handlers

    def handle_down(self, e):
        self.tabs[self.active_tab].scrolldown()
        self.draw()

    def handle_up(self, e):
        self.tabs[self.active_tab].scrollup()
        self.draw()

    def handle_enter(self, event):
        if self.focus == "address bar":
            self.tabs[self.active_tab].load(self.address_bar)
            self.focus = None
            self.draw()

    def handle_key(self, e):
        if len(e.char) == 0:
            return
        if not (0x20 <= ord(e.char) < 0x7F):
            return

        if self.focus == "address bar":
            self.address_bar += e.char
            self.draw()

    def handle_click(self, e):
        self.focus = None
        # if chrome has been clicked the browser handles it, otherwise it
        # delegates it to a tab
        if e.y < CHROME_PX:
            if 40 <= e.x < 40 + 80 * len(self.tabs) and 0 <= e.y < 40:
                self.active_tab = int((e.x - 40) / 80)
            elif 10 <= e.x < 30 and 10 <= e.y < 30:
                self.load("https://browser.engineering/")
            # go back button has been clicked
            elif 10 <= e.x < 35 and 50 <= e.y < 90:
                self.tabs[self.active_tab].go_back()
            elif 50 <= e.x < WIDTH - 10 and 50 <= e.y < 90:
                self.focus = "address bar"
                self.address_bar = ""
        else:
            self.tabs[self.active_tab].click(e.x, e.y - CHROME_PX)
        self.draw()


def parse_cli():
    import argparse

    parser = argparse.ArgumentParser(
        prog="webbrowser",
        usage=f"\n\tpython init.py [options] {SUPPORTED_SCHEMA}",
        description="A small webbrowser written in python.",
    )
    parser.add_argument("url", nargs="?", help="open a webpage at this url")
    # parser.add_argument("file", help="open a local file on the filesystem")
    return parser.parse_args()


def main():
    args = parse_cli()
    if not args.url == None:
        print("args url: ", args.url)
        Browser().load(args.url)
    else:
        path = os.path.abspath("resources/startpage.html")
        print("Path: file://" + path)
        Browser().load("file://" + path)

    tkinter.mainloop()


if __name__ == "__main__":
    main()
